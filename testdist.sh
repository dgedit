#!/bin/bash

if [ $# -lt 1 ]
then
		echo "Missing version argument."
		exit 1
fi

VER=$1

make dist && (
		mkdir -p tst
		cd tst
		rm -Rf dgedit-$VER
		tar xvzf ../dgedit-$VER.tar.gz
		cd dgedit-$VER
		./configure --prefix=/usr
		make
		DESTDIR=$PWD/tst/install make install
		(cd plugin; make install)
		make check
)
