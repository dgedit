/* -*- Mode: C++; tab-width: 2; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
/***************************************************************************
 *            instrumentdialog.cc
 *
 *  Wed May 23 17:35:09 CEST 2018
 *  Copyright 2018 Bent Bisballe Nyeng
 *  deva@aasimon.org
 ****************************************************************************/

/*
 *  This file is part of DrumGizmo.
 *
 *  DrumGizmo is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  DrumGizmo is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with DrumGizmo; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA.
 */
#include "instrumentdialog.h"

#include <QGridLayout>

#include <QLabel>
#include <QLineEdit>
#include <QPushButton>

#include <QFileDialog>
#include <QDialogButtonBox>

#include "project.h"

InstrumentDialog::InstrumentDialog(QWidget* parent, Instrument& instrument)
	: QDialog(parent)
	, instrument(instrument)
{
	setWindowModality(Qt::ApplicationModal);
	setWindowTitle(tr("Instrument Dialog"));
	setMinimumWidth(300);

	auto layout = new QGridLayout();
	setLayout(layout);

	int idx = 0;

	name = new QLineEdit();
	name->setText(instrument.getInstrumentName());
	layout->addWidget(new QLabel(tr("Name of the instrument:")), idx, 0, 1, 2);
	idx++;
	layout->addWidget(name, idx, 0);
	idx++;

	auto buttons =
		new QDialogButtonBox(QDialogButtonBox::Ok |
		                     QDialogButtonBox::Cancel |
		                     QDialogButtonBox::Apply);
	connect(buttons, SIGNAL(accepted()), this, SLOT(apply()));
	connect(buttons, SIGNAL(accepted()), this, SLOT(accept()));
	connect(buttons, SIGNAL(rejected()), this, SLOT(reject()));
	connect(buttons->button(QDialogButtonBox::Apply), SIGNAL(clicked()),
	        this, SLOT(apply()));
	layout->addWidget(buttons, idx, 0, 1, 2);
}

void InstrumentDialog::apply()
{
	// Only send out one update signal
	Project::RAIIBulkUpdate bulkUpdate(instrument.getProject());
	instrument.setInstrumentName(name->text());
}
