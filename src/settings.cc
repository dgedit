/* -*- Mode: C++; tab-width: 2; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
/***************************************************************************
 *            settings.cc
 *
 *  Sat May  5 11:57:00 CEST 2018
 *  Copyright 2018 Bent Bisballe Nyeng
 *  deva@aasimon.org
 ****************************************************************************/

/*
 *  This file is part of DrumGizmo.
 *
 *  DrumGizmo is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  DrumGizmo is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with DrumGizmo; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA.
 */
#include "settings.h"

#include <QSettings>

Settings::Settings()
{
}

Settings::~Settings()
{
}

void Settings::loadGeometry(QByteArray &state, QByteArray &geometry) const
{
	QSettings settings;

	settings.beginGroup("MainWindow");
	state = settings.value("state").toByteArray();
	geometry = settings.value("geometry").toByteArray();
	settings.endGroup();
}

void Settings::saveGeometry(const QByteArray &state, const QByteArray &geometry)
{
	QSettings settings;

	settings.beginGroup("MainWindow");
	settings.setValue("state", state);
	settings.setValue("geometry", geometry);
	settings.endGroup();
}

QString Settings::loadExportPath() const
{
	QSettings settings;
	settings.beginGroup("MainWindow");
	QString export_path = settings.value("exportpath", "").toString();
	settings.endGroup();
	return export_path;
}

void Settings::saveExportPath(const QString& export_path)
{
	QSettings settings;
	settings.beginGroup("MainWindow");
	settings.setValue("exportpath", export_path);
	settings.endGroup();
}
