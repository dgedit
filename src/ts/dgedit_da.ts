<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="da_DK">
<context>
    <name>AboutDialog</name>
    <message>
        <location filename="../aboutdialog.cc" line="43"/>
        <source>About DGEdit</source>
        <translation>Om DGEdit</translation>
    </message>
    <message>
        <location filename="../aboutdialog.cc" line="81"/>
        <source>&lt;h1&gt;DGEdit&lt;/h1&gt;&lt;h2&gt;v.</source>
        <translation>&lt;h1&gt;DGEdit&lt;/h1&gt;&lt;h2&gt;v.</translation>
    </message>
</context>
<context>
    <name>CanvasToolListen</name>
    <message>
        <location filename="../canvastoollisten.h" line="41"/>
        <source>Listen</source>
        <translation>Lyt</translation>
    </message>
</context>
<context>
    <name>CanvasToolSelections</name>
    <message>
        <location filename="../canvastoolselections.h" line="45"/>
        <source>Selections</source>
        <translation>Markeringer</translation>
    </message>
</context>
<context>
    <name>CanvasToolThreshold</name>
    <message>
        <location filename="../canvastoolthreshold.h" line="44"/>
        <source>Threshold</source>
        <translation>Grænseværdi</translation>
    </message>
</context>
<context>
    <name>ChannelDialog</name>
    <message>
        <location filename="../channeldialog.cc" line="45"/>
        <source>Channel Dialog</source>
        <translation>Kanaler</translation>
    </message>
    <message>
        <location filename="../channeldialog.cc" line="55"/>
        <source>Name of the channel:</source>
        <translation>Kanalens navn:</translation>
    </message>
</context>
<context>
    <name>ChannelMapDeligate</name>
    <message>
        <location filename="../filelist.cc" line="104"/>
        <source>&lt;None&gt;</source>
        <translation>&lt;Ingen&gt;</translation>
    </message>
</context>
<context>
    <name>FileDataModel</name>
    <message>
        <location filename="../filelist.cc" line="309"/>
        <source>&lt;None&gt;</source>
        <translation>&lt;Ingen&gt;</translation>
    </message>
    <message>
        <location filename="../filelist.cc" line="341"/>
        <source>M</source>
        <translation>M</translation>
    </message>
    <message>
        <location filename="../filelist.cc" line="342"/>
        <source>Filename</source>
        <translation>Filnavn</translation>
    </message>
    <message>
        <location filename="../filelist.cc" line="343"/>
        <source>m</source>
        <translation>m</translation>
    </message>
    <message>
        <location filename="../filelist.cc" line="344"/>
        <source>Name</source>
        <translation>Navn</translation>
    </message>
    <message>
        <location filename="../filelist.cc" line="345"/>
        <source>Kit Channel</source>
        <translation>Trommesæt kanal</translation>
    </message>
</context>
<context>
    <name>FileList</name>
    <message>
        <location filename="../filelist.cc" line="462"/>
        <source>Open file</source>
        <translation>Åbn fil</translation>
    </message>
    <message>
        <location filename="../filelist.cc" line="463"/>
        <source>Audio Files (*.wav)</source>
        <translation>Lyd filer (*.wav)</translation>
    </message>
    <message>
        <location filename="../filelist.cc" line="530"/>
        <source>Set as Master (dbl-click)</source>
        <translation>Vælg hovedkanal (dbl klik)</translation>
    </message>
    <message>
        <source>Edit name</source>
        <translation type="vanished">Redigér navn</translation>
    </message>
    <message>
        <location filename="../filelist.cc" line="533"/>
        <source>Remove</source>
        <translation>Fjern</translation>
    </message>
    <message>
        <location filename="../filelist.cc" line="536"/>
        <source>Remove all</source>
        <translation>Fjern alle</translation>
    </message>
</context>
<context>
    <name>InstrumentDialog</name>
    <message>
        <location filename="../instrumentdialog.cc" line="45"/>
        <source>Instrument Dialog</source>
        <translation>Instrument</translation>
    </message>
    <message>
        <location filename="../instrumentdialog.cc" line="55"/>
        <source>Name of the instrument:</source>
        <translation>Navn på instrumentet:</translation>
    </message>
</context>
<context>
    <name>InstrumentWidget</name>
    <message>
        <location filename="../instrumentwidget.cc" line="92"/>
        <source>Tools</source>
        <translation>Værktøjer</translation>
    </message>
    <message>
        <location filename="../instrumentwidget.cc" line="123"/>
        <source>Play samples</source>
        <translation>Afspil lydbidder</translation>
    </message>
    <message>
        <location filename="../instrumentwidget.cc" line="137"/>
        <source>Process</source>
        <translation>Processér</translation>
    </message>
    <message>
        <location filename="../instrumentwidget.cc" line="146"/>
        <source>Files</source>
        <translation>Filer</translation>
    </message>
    <message>
        <location filename="../instrumentwidget.cc" line="147"/>
        <location filename="../instrumentwidget.cc" line="272"/>
        <source>Generate</source>
        <translation>Generér</translation>
    </message>
    <message>
        <location filename="../instrumentwidget.cc" line="148"/>
        <source>Edit</source>
        <translation>Redigér</translation>
    </message>
    <message>
        <location filename="../instrumentwidget.cc" line="193"/>
        <source>Files: (double-click to set as master)</source>
        <translation>Filer: (dobbeltklik for at vælge som hovedkanal)</translation>
    </message>
    <message>
        <location filename="../instrumentwidget.cc" line="195"/>
        <source>Add files...</source>
        <translation>Tilføj filer...</translation>
    </message>
    <message>
        <location filename="../instrumentwidget.cc" line="284"/>
        <source>Clear</source>
        <translation>Ryd</translation>
    </message>
    <message>
        <location filename="../instrumentwidget.cc" line="293"/>
        <source>Attack length:</source>
        <translation>Attack længde:</translation>
    </message>
    <message>
        <location filename="../instrumentwidget.cc" line="302"/>
        <source>Power spread:</source>
        <translation>Energi spredning:</translation>
    </message>
    <message>
        <location filename="../instrumentwidget.cc" line="311"/>
        <source>Minimum size (samples):</source>
        <translation>Minimum størrelse (samples):</translation>
    </message>
    <message>
        <location filename="../instrumentwidget.cc" line="320"/>
        <source>Falloff:</source>
        <translation>Falloff:</translation>
    </message>
    <message>
        <location filename="../instrumentwidget.cc" line="329"/>
        <source>Fadelength:</source>
        <translation>Fadelængde:</translation>
    </message>
    <message>
        <location filename="../instrumentwidget.cc" line="359"/>
        <source>Prefix:</source>
        <translation>Præfix:</translation>
    </message>
    <message>
        <location filename="../instrumentwidget.cc" line="366"/>
        <source>Export path:</source>
        <translation>Eksporteringssti:</translation>
    </message>
    <message>
        <location filename="../instrumentwidget.cc" line="373"/>
        <source>...</source>
        <translation>...</translation>
    </message>
    <message>
        <location filename="../instrumentwidget.cc" line="380"/>
        <source>Export</source>
        <translation>Eksportér</translation>
    </message>
    <message>
        <location filename="../instrumentwidget.cc" line="475"/>
        <source>Loading...</source>
        <translation>Indlæser...</translation>
    </message>
    <message>
        <location filename="../instrumentwidget.cc" line="486"/>
        <source>Ready</source>
        <translation>Klar</translation>
    </message>
    <message>
        <location filename="../instrumentwidget.cc" line="495"/>
        <source>Select export path</source>
        <translation>Vælg eksporteringssti</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <source>Tools</source>
        <translation type="vanished">Værktøjer</translation>
    </message>
    <message>
        <location filename="../mainwindow.cc" line="67"/>
        <source>&amp;File</source>
        <translation>&amp;Filer</translation>
    </message>
    <message>
        <location filename="../mainwindow.cc" line="69"/>
        <source>&amp;New Project</source>
        <translation>&amp;Nyt projekt</translation>
    </message>
    <message>
        <location filename="../mainwindow.cc" line="73"/>
        <source>&amp;Load Project...</source>
        <translation>&amp;Indlæs projekt...</translation>
    </message>
    <message>
        <location filename="../mainwindow.cc" line="77"/>
        <source>&amp;Save Project</source>
        <translation>&amp;Gem projekt</translation>
    </message>
    <message>
        <location filename="../mainwindow.cc" line="82"/>
        <source>Save Project As...</source>
        <translation>Gem projekt som...</translation>
    </message>
    <message>
        <location filename="../mainwindow.cc" line="87"/>
        <source>&amp;Quit</source>
        <translation>&amp;Afslut</translation>
    </message>
    <message>
        <location filename="../mainwindow.cc" line="91"/>
        <source>&amp;Project</source>
        <translation>&amp;Projekt</translation>
    </message>
    <message>
        <location filename="../mainwindow.cc" line="92"/>
        <source>&amp;Export...</source>
        <translation>&amp;Eksportér...</translation>
    </message>
    <message>
        <location filename="../mainwindow.cc" line="96"/>
        <source>&amp;Edit Project...</source>
        <translation>&amp;Redigér projekt...</translation>
    </message>
    <message>
        <location filename="../mainwindow.cc" line="105"/>
        <source>Help</source>
        <translation>Hjælp</translation>
    </message>
    <message>
        <location filename="../mainwindow.cc" line="106"/>
        <source>About</source>
        <translation>Om</translation>
    </message>
    <message>
        <location filename="../mainwindow.cc" line="110"/>
        <source>Instruments:</source>
        <translation>Instrumenter:</translation>
    </message>
    <message>
        <location filename="../mainwindow.cc" line="142"/>
        <source>Kit channels:</source>
        <translation>Trommesæt kanaler:</translation>
    </message>
    <message>
        <location filename="../mainwindow.cc" line="229"/>
        <source>Delete Instrument</source>
        <translation>Slet instrument</translation>
    </message>
    <message>
        <location filename="../mainwindow.cc" line="230"/>
        <source>Are you sure you want to delete the selected instrument(s)?</source>
        <translation>Er du sikkert på at du vil slette det/de valgte instrument(er)?</translation>
    </message>
    <message>
        <location filename="../mainwindow.cc" line="298"/>
        <source>Delete Channel</source>
        <translation>Slet kanal</translation>
    </message>
    <message>
        <location filename="../mainwindow.cc" line="299"/>
        <source>Are you sure you want to delete the selected channel(s)?</source>
        <translation>Er du sikker på at du vil slette den/de valgte kanal(er)?</translation>
    </message>
    <message>
        <location filename="../mainwindow.cc" line="337"/>
        <source>Channels</source>
        <translation>Kanaler</translation>
    </message>
    <message>
        <location filename="../mainwindow.cc" line="348"/>
        <source>[Untitled Project]</source>
        <translation>[Unavngivet projekt]</translation>
    </message>
    <message>
        <location filename="../mainwindow.cc" line="383"/>
        <source>Save before closing project?</source>
        <translation>Gem projektet før det lukkes?</translation>
    </message>
    <message>
        <location filename="../mainwindow.cc" line="384"/>
        <source>The project has changed. Do you want to save before closing?</source>
        <translation>Projektet er ændret. Vil du gemme før du lukker?</translation>
    </message>
    <message>
        <location filename="../mainwindow.cc" line="473"/>
        <source>Load DGEdit Project</source>
        <translation>Indlæs DGEdit projekt</translation>
    </message>
    <message>
        <location filename="../mainwindow.cc" line="475"/>
        <location filename="../mainwindow.cc" line="566"/>
        <source>DGEdit Project Files (*.dgedit)</source>
        <translation>DGEdit projekt filer (*.dgedit)</translation>
    </message>
    <message>
        <location filename="../mainwindow.cc" line="530"/>
        <source>Loaded</source>
        <translation>Indlæst</translation>
    </message>
    <message>
        <location filename="../mainwindow.cc" line="558"/>
        <source>Saved</source>
        <translation>Gemt</translation>
    </message>
    <message>
        <location filename="../mainwindow.cc" line="564"/>
        <source>Save DGEdit Project</source>
        <translation>Gem DGEdit projekt</translation>
    </message>
    <message>
        <source>Play samples</source>
        <translation type="vanished">Afspil lydbidder</translation>
    </message>
    <message>
        <source>Dock Widget</source>
        <translation type="vanished">Dock vindue</translation>
    </message>
    <message>
        <source>Files</source>
        <translation type="vanished">Filer</translation>
    </message>
    <message>
        <source>Edit</source>
        <translation type="vanished">Redigér</translation>
    </message>
    <message>
        <source>Export</source>
        <translation type="vanished">Eksportér</translation>
    </message>
    <message>
        <location filename="../mainwindow.cc" line="171"/>
        <source>Ready</source>
        <translation>Klar</translation>
    </message>
    <message>
        <source>Files: (double-click to set as master)</source>
        <translation type="vanished">Filer: (dobbeltklik for at vælge som hovedkanal)</translation>
    </message>
    <message>
        <source>Add files...</source>
        <translation type="vanished">Tilføj filer...</translation>
    </message>
    <message>
        <source>Generate</source>
        <translation type="vanished">Generér</translation>
    </message>
    <message>
        <source>Clear</source>
        <translation type="vanished">Ryd</translation>
    </message>
    <message>
        <source>Attack length:</source>
        <translation type="vanished">Attack længde:</translation>
    </message>
    <message>
        <source>Power spread:</source>
        <translation type="vanished">Energi spredning:</translation>
    </message>
    <message>
        <source>Minimum size (samples):</source>
        <translation type="vanished">Minimum størrelse (samples):</translation>
    </message>
    <message>
        <source>Falloff:</source>
        <translation type="vanished">Falloff:</translation>
    </message>
    <message>
        <source>Fadelength:</source>
        <translation type="vanished">Fadelængde:</translation>
    </message>
    <message>
        <source>Prefix:</source>
        <translation type="vanished">Prefix:</translation>
    </message>
    <message>
        <source>Export path:</source>
        <translation type="vanished">Eksporteringssti:</translation>
    </message>
    <message>
        <source>...</source>
        <translation type="vanished">...</translation>
    </message>
    <message>
        <source>Loading...</source>
        <translation type="vanished">Indlæser...</translation>
    </message>
    <message>
        <source>Select export path</source>
        <translation type="vanished">Vælg eksporteringssti</translation>
    </message>
</context>
<context>
    <name>ProjectDialog</name>
    <message>
        <location filename="../projectdialog.cc" line="46"/>
        <source>Project Dialog</source>
        <translation>Projekt</translation>
    </message>
    <message>
        <location filename="../projectdialog.cc" line="56"/>
        <source>Name of the project:</source>
        <translation>Navn på projektet:</translation>
    </message>
    <message>
        <location filename="../projectdialog.cc" line="63"/>
        <source>...</source>
        <translation>...</translation>
    </message>
    <message>
        <location filename="../projectdialog.cc" line="66"/>
        <source>Base path of raw files:</source>
        <translation>Basis sti for de rå filer:</translation>
    </message>
    <message>
        <location filename="../projectdialog.cc" line="74"/>
        <source>Description of the project:</source>
        <translation>Beskrivelse af projektet:</translation>
    </message>
    <message>
        <location filename="../projectdialog.cc" line="95"/>
        <source>Choose base directory with the raw sample files.</source>
        <translation>Vælg basis sti for de rå lydfiler.</translation>
    </message>
</context>
<context>
    <name>ProjectRenderer</name>
    <message>
        <location filename="../projectrenderer.cc" line="84"/>
        <source>Writing instrument: </source>
        <translation>Skriver instrument:</translation>
    </message>
</context>
<context>
    <name>RenderDialog</name>
    <message>
        <location filename="../renderdialog.cc" line="280"/>
        <source>Export drumkit</source>
        <translation>Eksportér trommesæt</translation>
    </message>
    <message>
        <location filename="../renderdialog.cc" line="286"/>
        <source>Export path:</source>
        <translation>Eksporteringssti:</translation>
    </message>
    <message>
        <location filename="../renderdialog.cc" line="294"/>
        <source>...</source>
        <translation>...</translation>
    </message>
    <message>
        <location filename="../renderdialog.cc" line="303"/>
        <source>Export</source>
        <translation>Eksportér</translation>
    </message>
    <message>
        <location filename="../renderdialog.cc" line="405"/>
        <source>Choose export directory.</source>
        <translation>Vækg eksporteringssti.</translation>
    </message>
</context>
<context>
    <name>SelectionEditor</name>
    <message>
        <location filename="../selectioneditor.cc" line="60"/>
        <source>From:</source>
        <translation>Fra:</translation>
    </message>
    <message>
        <location filename="../selectioneditor.cc" line="61"/>
        <source>To:</source>
        <translation>Til:</translation>
    </message>
    <message>
        <location filename="../selectioneditor.cc" line="62"/>
        <source>FadeIn:</source>
        <translation>FadeIn:</translation>
    </message>
    <message>
        <location filename="../selectioneditor.cc" line="63"/>
        <source>FadeOut:</source>
        <translation>FadeOut:</translation>
    </message>
    <message>
        <location filename="../selectioneditor.cc" line="64"/>
        <source>Energy:</source>
        <translation>Energi:</translation>
    </message>
    <message>
        <location filename="../selectioneditor.cc" line="65"/>
        <source>Name:</source>
        <translation>Navn:</translation>
    </message>
</context>
<context>
    <name>VolumeFader</name>
    <message>
        <location filename="../volumefader.cc" line="73"/>
        <source>Peak </source>
        <translation>Peak </translation>
    </message>
    <message>
        <location filename="../volumefader.cc" line="97"/>
        <source>Gain %1 dB</source>
        <translation>Forstærkning %1 dB</translation>
    </message>
</context>
</TS>
