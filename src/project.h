/* -*- Mode: C++; tab-width: 2; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
/***************************************************************************
 *            project.h
 *
 *  Sun May  6 11:38:11 CEST 2018
 *  Copyright 2018 Bent Bisballe Nyeng
 *  deva@aasimon.org
 ****************************************************************************/

/*
 *  This file is part of DrumGizmo.
 *
 *  DrumGizmo is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  DrumGizmo is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with DrumGizmo; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA.
 */
#pragma once

#include <list>

#include <QObject>
#include <QString>

#include "audioextractor.h"
#include "selection.h"

class Project;
class Instrument;

class AudioFile
{
public:
	AudioFile(Instrument& instrument, int id);

	int getId() const;

	QString getFile() const;
	void setFile(const QString& file);
	QString getAbsoluteFile() const;

	QString getName() const;
	void setName(const QString& name);

	int getChannelMapId() const;
	void setChannelMapId(int id);

	bool getMainChannel() const;
	void setMainChannel(bool main);

private:
	friend class ProjectSerialiser;

	QString file;
	QString name;
	int channel_map_id;
	bool main_channel;

	int id;
	Instrument& instrument;
};

class Instrument
{
public:
	Instrument(Project& project, int id);

	int getId() const;

	QString getInstrumentName() const;
	void setInstrumentName(const QString& instrument_name);

	QString getMasterFile() const;
	void setMasterFile(const QString& master_file);

	AudioFile& getAudioFile(int id);
	int createAudioFile();
	void deleteAudioFile(int id);
	QList<int> getAudioFileList() const;

	std::size_t getAttackLength() const;
	void setAttackLength(std::size_t attack_length);

	std::size_t getPowerSpread() const;
	void setPowerSpread(std::size_t power_spread);

	std::size_t getMinimumSize() const;
	void setMinimumSize(std::size_t minimum_size);

	std::size_t getFalloff() const;
	void setFalloff(std::size_t falloff);

	std::size_t getFadeLength() const;
	void setFadeLength(std::size_t fade_length);

	float getThreshold() const;
	void setThreshold(float threshold);

	Selections getSelections() const;
	void setSelections(const Selections& selectios);

	QString getPrefix() const;
	void setPrefix(const QString& prefix);

	Project& getProject();

private:
	friend class ProjectSerialiser;

	int id;
	QString instrument_name;

	// Files tab
	QString master_file;
	std::list<AudioFile> audio_files;

	// Generate tab
	std::size_t attack_length{300};
	std::size_t power_spread{1000};
	std::size_t minimum_size{100};
	std::size_t falloff{300};
	std::size_t fade_length{666};

	// Canvas
	float threshold{0.5f};

	// Edit tab
	Selections selections;

	// Export tab
	QString prefix;

	Project& project;
};

class Channel
{
public:
	Channel(Project& project, int id);

	int getId() const;

	QString getChannelName() const;
	void setChannelName(const QString& channel_name);

	Project& getProject();

private:
	friend class ProjectSerialiser;

	int id;

	QString channel_name;

	Project& project;
};

class Project
	: public QObject
{
	Q_OBJECT
public:
	void bulkUpdateBegin();
	void bulkUpdateEnd();

	class RAIIBulkUpdate
	{
	public:
		RAIIBulkUpdate(Project& project)
			: project(project)
		{
			project.bulkUpdateBegin();
		}
		~RAIIBulkUpdate()
		{
			project.bulkUpdateEnd();
		}

	private:
		Project& project;
	};

	QString getProjectFile() const;
	void setProjectFile(const QString& project_file);

	QString getProjectName() const;
	void setProjectName(const QString& project_name);

	QString getRawFileRoot() const;
	void setRawFileRoot(const QString& raw_file_root);

	QString getExportPath() const;
	void setExportPath(const QString& prefix);

	QString getProjectDescription() const;
	void setProjectDescription(const QString& description);

	double getProjectSamplerate() const;
	void setProjectSamplerate(double samplerate);

	Instrument& getInstrument(int id);
	int createInstrument();
	void deleteInstrument(int id);
	QList<int> getInstrumentList() const;

	Channel& getChannel(int id);
	int createChannel();
	void deleteChannel(int id);
	QList<int> getChannelList() const;

	void reset();

signals:
	void projectChanged();

private:
	friend class ProjectSerialiser;
	friend class Instrument;

	QString project_file;
	QString project_name;
	QString raw_file_root;
	QString export_path;
	QString description;
	double samplerate{-1.0};

	std::list<Instrument> instruments;
	std::list<Channel> channels;
	int next_id{0};

	int update_count{0};
};
