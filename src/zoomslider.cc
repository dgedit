/* -*- Mode: C++; tab-width: 2; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
/***************************************************************************
 *            zoomslider.cc
 *
 *  Fri May  2 21:23:26 CEST 2014
 *  Copyright 2014 Bent Bisballe Nyeng
 *  deva@aasimon.org
 ****************************************************************************/

/*
 *  This file is part of DrumGizmo.
 *
 *  DrumGizmo is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  DrumGizmo is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with DrumGizmo; if not, write to the 45Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA.
 */
#include "zoomslider.h"

#include <QPainter>

ZoomSlider::ZoomSlider(Qt::Orientation orientation)
	: QSlider(orientation)
{
	inverted = false;
	from = 0;
	to = 1;
	tick_width = 0.1;

	connect(this, SIGNAL(valueChanged(int)), this, SLOT(sliderValueChanged(int)));
}

void ZoomSlider::paintEvent(QPaintEvent *ev)
{
	/*
	  QPainter p(this);
	  p.drawLine(0, 0, width(), height());
	*/
	QSlider::paintEvent(ev);
}

void ZoomSlider::setRange(float from, float to)
{
	this->inverted = false;
	this->from = from;
	this->to = to;

	int i_from = from / tick_width;
	int i_to = to / tick_width;

	if(i_from > i_to)
	{
		this->inverted = true;
		int tmp;
		tmp = i_from;
		i_from = i_to;
		i_to = tmp;
	}

	QSlider::setRange(i_from, i_to);
}

float ZoomSlider::fromSlider(int value)
{
	float val = (float)value * tick_width;

	if(inverted)
	{
		val *= -1;
		val = val - to + from;

		if(value == QSlider::minimum())
		{
			val = from;
		}
		if(value == QSlider::maximum())
		{
			val = to;
		}
	}
	else
	{
		if(value == QSlider::minimum())
		{
			val = from;
		}
		if(value == QSlider::maximum())
		{
			val = to;
		}
	}

	return val;
}

void ZoomSlider::sliderValueChanged(int value)
{
	emit valueChanged(fromSlider(value));
}

void ZoomSlider::setTickWidth(float tick_width)
{
	this->tick_width = tick_width;
	setRange(from, to); // Update slider value span.
}

void ZoomSlider::setValue(float value)
{
	int i_value = value * tick_width;
	if(inverted)
	{
		i_value *= -1;
		i_value += from;
	}
	QSlider::setValue(i_value);
}
