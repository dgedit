/* -*- Mode: C++; tab-width: 2; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
/***************************************************************************
 *            instrumentwidget.h
 *
 *  Sat May 12 15:38:38 CEST 2018
 *  Copyright 2018 Bent Bisballe Nyeng
 *  deva@aasimon.org
 ****************************************************************************/

/*
 *  This file is part of DrumGizmo.
 *
 *  DrumGizmo is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  DrumGizmo is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with DrumGizmo; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA.
 */
#pragma once

#include <QMainWindow>
#include <QScrollBar>
#include <QComboBox>
#include <QSlider>
#include <QLineEdit>
#include <QCloseEvent>
#include <QTabWidget>

#include "canvas.h"
#include "audioextractor.h"
#include "samplesorter.h"
#include "filelist.h"
#include "canvastoolselections.h"
#include "canvastoolthreshold.h"
#include "canvastoollisten.h"
#include "selection.h"
#include "player.h"
#include "zoomslider.h"
#include "canvaswidget.h"

class Instrument;
class Settings;

class Preset
{
public:
	QString prefix;
	int attacklength;
	int falloff;
	int fadelength;
};
Q_DECLARE_METATYPE(Preset)

class InstrumentWidget
	: public QMainWindow
{
	Q_OBJECT
public:
	InstrumentWidget(Settings& settings, Instrument& instrument);
	~InstrumentWidget();

public slots:
	void doExport();
	void loadFile(QString filename);
	void playSamples();
	void browse();
	void tabChanged(int tabid);
	void generateSlidersChanged();
	void selectionChanged();
	void prefixChanged();
	void exportPathChanged();

private:
	QWidget* createFilesTab();
	QWidget* createEditTab();
	QWidget* createGenerateTab();
	QWidget* createExportTab();

	int generateTabId;

	SampleSorter* sorter;
	CanvasToolSelections* tool_selections;
	CanvasToolThreshold* threshold;
	CanvasToolListen* listen;
	AudioExtractor* extractor;
	FileList* filelist;

	CanvasWidget* canvaswidget;

	QScrollBar* sb_playsamples;
	QComboBox* presets;
	QSlider* slider_attacklength;
	QSlider* slider_spread;
	QSlider* slider_hold;
	QSlider* slider_falloff;
	QSlider* slider_fadelength;
	QLineEdit* prefix;
	QLineEdit* lineed_exportp;

	QTabWidget* tabs;

	// Session state information:
	Selections selections;
	Selections selections_preview;
	Player player;

	Settings& settings;
	Instrument& instrument;
};
