/* -*- Mode: C++; tab-width: 2; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
/***************************************************************************
 *            projectdialog.cc
 *
 *  Sat May  5 14:54:18 CEST 2018
 *  Copyright 2018 Bent Bisballe Nyeng
 *  deva@aasimon.org
 ****************************************************************************/

/*
 *  This file is part of DrumGizmo.
 *
 *  DrumGizmo is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  DrumGizmo is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with DrumGizmo; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA.
 */
#include "projectdialog.h"

#include <QGridLayout>

#include <QLabel>
#include <QLineEdit>
#include <QTextEdit>
#include <QPushButton>

#include <QFileDialog>
#include <QDialogButtonBox>

#include "project.h"

ProjectDialog::ProjectDialog(QWidget* parent, Project& project)
	: QDialog(parent)
	, project(project)
{
	setWindowModality(Qt::ApplicationModal);
	setWindowTitle(tr("Project Dialog"));
	setMinimumWidth(300);

	auto layout = new QGridLayout();
	setLayout(layout);

	int idx = 0;

	name = new QLineEdit();
	name->setText(project.getProjectName());
	layout->addWidget(new QLabel(tr("Name of the project:")), idx, 0, 1, 2);
	idx++;
	layout->addWidget(name, idx, 0, 1, 2);
	idx++;

	raw_dir = new QLineEdit();
	raw_dir->setText(project.getRawFileRoot());
	auto btn = new QPushButton(tr("..."));
	btn->setMaximumWidth(32);
	connect(btn, SIGNAL(clicked()), this, SLOT(chooseRawFilesDir()));
	layout->addWidget(new QLabel(tr("Base path of raw files:")), idx, 0, 1, 2);
	idx++;
	layout->addWidget(raw_dir, idx, 0);
	layout->addWidget(btn, idx, 1);
	idx++;

	description = new QTextEdit();
	description->setText(project.getProjectDescription());
	layout->addWidget(new QLabel(tr("Description of the project:")), idx, 0, 1, 2);
	idx++;
	layout->addWidget(description, idx, 0, 1, 2);
	idx++;

	auto buttons =
		new QDialogButtonBox(QDialogButtonBox::Ok |
		                     QDialogButtonBox::Cancel |
		                     QDialogButtonBox::Apply);
	connect(buttons, SIGNAL(accepted()), this, SLOT(apply()));
	connect(buttons, SIGNAL(accepted()), this, SLOT(accept()));
	connect(buttons, SIGNAL(rejected()), this, SLOT(reject()));
	connect(buttons->button(QDialogButtonBox::Apply), SIGNAL(clicked()),
	        this, SLOT(apply()));
	layout->addWidget(buttons, idx, 0, 1, 2);
}

void ProjectDialog::chooseRawFilesDir()
{
	QString path =
		QFileDialog::getExistingDirectory(
			this, tr("Choose base directory with the raw sample files."),
			raw_dir->text(), QFileDialog::ShowDirsOnly);

	if(path != "")
	{
		raw_dir->setText(path);
	}
}

void ProjectDialog::apply()
{
	// Only send out one update signal
	Project::RAIIBulkUpdate bulkUpdate(project);
	project.setProjectName(name->text());
	project.setRawFileRoot(raw_dir->text());
	project.setProjectDescription(description->toPlainText());
}
