/* -*- Mode: C++; tab-width: 2; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
/***************************************************************************
 *            editor.cc
 *
 *  Tue Nov 10 08:37:43 CET 2009
 *  Copyright 2009 Bent Bisballe Nyeng
 *  deva@aasimon.org
 ****************************************************************************/

/*
 *  This file is part of DrumGizmo.
 *
 *  DrumGizmo is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  DrumGizmo is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with DrumGizmo; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA.
 */
#include <QApplication>

#include "mainwindow.h"
#include "settings.h"
#include "localehandler.h"

int main(int argc, char *argv[])
{
	QApplication app(argc, argv);

	LocaleHandler locale(app);

	Settings settings;
	MainWindow mainwindow(settings);
	mainwindow.show();

	if(argc > 1)
	{
		QFile file(argv[1]);
		if(file.exists())
		{
			mainwindow.loadProject(argv[1]);
		}
	}

	return app.exec();
}
