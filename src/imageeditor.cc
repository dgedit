/* -*- Mode: C++; tab-width: 2; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
/***************************************************************************
 *            imageeditor.cc
 *
 *  Tue Jul 24 11:55:43 CEST 2018
 *  Copyright 2018 Bent Bisballe Nyeng
 *  deva@aasimon.org
 ****************************************************************************/

/*
 *  This file is part of DrumGizmo.
 *
 *  DrumGizmo is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  DrumGizmo is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with DrumGizmo; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA.
 */
#include "imageeditor.h"

#include <QLabel>

ImageEditor::ImageEditor(QWidget* parent)
	: QWidget(parent)
{
}

ImageEditor::~ImageEditor()
{
}

void ImageEditor::setImage(QString filename)
{
	QLabel *image = new QLabel(this);
	QPixmap pic(filename);
	QPixmap scaled = pic.scaled(width(), height(),
	                            Qt::KeepAspectRatio, Qt::FastTransformation);
	image->setPixmap(scaled);
	image->move(0, 0);
	image->resize(width(), height());
}
