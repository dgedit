/* -*- Mode: C++; tab-width: 2; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
/***************************************************************************
 *            projectdialog.h
 *
 *  Sat May  5 14:54:18 CEST 2018
 *  Copyright 2018 Bent Bisballe Nyeng
 *  deva@aasimon.org
 ****************************************************************************/

/*
 *  This file is part of DrumGizmo.
 *
 *  DrumGizmo is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  DrumGizmo is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with DrumGizmo; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA.
 */
#pragma once

#include <QDialog>

class QLineEdit;
class QTextEdit;

class Project;

class ProjectDialog
	: public QDialog
{
	Q_OBJECT
public:
	ProjectDialog(QWidget* parent, Project& project);
	~ProjectDialog() = default;

private slots:
	void chooseRawFilesDir();
	void apply();

private:
	QLineEdit* name{nullptr};
	QLineEdit* raw_dir{nullptr};
	QTextEdit* description{nullptr};

	Project& project;
};
